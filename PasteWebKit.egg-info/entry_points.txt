
    [paste.app_factory]
    main=paste.webkit.wsgiapp:make_webkit_app

    [paste.paster_command]
    servlet=paste.webkit.servlet_script:ServletCommand

    [paste.paster_create_template]
    webkit=paste.webkit.templates:WebKit
    