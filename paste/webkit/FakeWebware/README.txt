This directory can be added to sys.path so that all your Webware
imports will still work, but will import WSGIWebKit versions of the
objects.

Items will be added here on an as-needed basis.  I don't want to bring
every public object from Webware into this setup; in part because many
of them are not currently implemented.
