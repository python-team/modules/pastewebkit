pastewebkit (1.0-9) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Sandro Tosi ]
  * Use the new Debian Python Team contact name and address

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 17:01:38 -0500

pastewebkit (1.0-8) unstable; urgency=medium

  * Team upload.

  [ SVN-Git Migration ]
  * Update Vcs fields for git migration.
  * Init git-dpm.

  [ Ondřej Nový ]
  * Use HTTPS in VCS URLs.

  [ Mattia Rizzolo ]
  * Bump debhelper compat level to 10.
  * Rewrite debian/rules using the dh sequencer and pybuild.  Closes: #833227
  * wrap-and-sort.
  * Bump Standards-Version to 3.9.8, no changes needed.
  * Ignore changes to the egg-info directory, fixing FTBFS when built twice in
    a row.  Closes: #671215

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 02 Apr 2017 21:15:56 +0200

pastewebkit (1.0-7) unstable; urgency=low

  * Remove .pth file (introduced in last upload by accident)
  * Add build-indep and build-arch targets in debian/rules

 -- Piotr Ożarowski <piotr@debian.org>  Thu, 22 Sep 2011 21:23:41 +0200

pastewebkit (1.0-6) unstable; urgency=low

  * Convert to dh_python2
  * Source format changed to 3.0 (quilt)
  * Standards-Version bumped to 3.9.2 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 28 May 2011 18:12:19 +0200

pastewebkit (1.0-5) unstable; urgency=low

  * Move paster's templates to /usr/share/paster_templates/
  * Add cast_sys.version_info_to_tuple patch as Python2.7's sys.version_info
    is not pickable (it's a named tuple in Python >= 2.7) - closes: #606616
    Thanks to Jakub Wilk for his outstanding quality assurance work on Python
    in Debian
  * Change source package format to 3.0 (quilt)
  * Change Debian packaging license to MIT (to match upstream)
  * Standards-Version bumped to 3.9.1 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 11 Dec 2010 15:23:32 +0100

pastewebkit (1.0-4) unstable; urgency=low

  [ Sandro Tosi ]
  * Switch Vcs-Browser field to viewsvn

  [ Piotr Ożarowski ]
  * Make sure all Python versions use site-packages as temporary
    installation directory for modules (closes: #547876)
  * Standards-Version bumped to 3.8.3 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Fri, 25 Sep 2009 22:57:39 +0200

pastewebkit (1.0-3) unstable; urgency=low

  * Remove paste-common dependency, python-support will provide all needed
    namespace, require python-paste (>= 1.7-1~) instead
  * Replace python-setuptools runtime dependency with new python-pkg-resources
    (closes: 468712)
  * Change setuptools' required build version to (>= 0.6b3-1~) to ease
    backports
  * Add info about files in paste/webkit/FakeWebware to debian/copyright

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 24 May 2008 20:42:06 +0200

pastewebkit (1.0-2) unstable; urgency=low

  * Switch to python-support. All paste packages will now depend on
    paste-common (>= 1.6-1) to make sure all use pysupport
  * Compress binary package with bzip2
  * Vcs-Svn, Vcs-Browser and Homepage fields added
  * Changed my address to piotr@debian.org
  * Bump Standards-Version to 3.7.3 (no changes needed)

 -- Piotr Ożarowski <piotr@debian.org>  Sat, 05 Jan 2008 21:43:51 +0100

pastewebkit (1.0-1) unstable; urgency=low

  * New upstream release
  * Documentation changes:
    - Files are now generated from sources
    - Add link in python-paste's docs
    - Add python-docutils in build dependencies
  * debian/rules cleaned:
    + Fixed a little bashism
    + dh_python removed, dh_pycentral will generate all data now
    + Unneeded dh_installexamples, dh_strip and dh_shlibdeps calls removed
  * debian/control:
    + bumped debhelper and python-central required versions due to dh_python
      call removal
    + Homepage updated
  * Added debian/pycompat file
  * Test files are no longer installed as examples

 -- Piotr Ozarowski <ozarow@gmail.com>  Mon, 23 Oct 2006 17:36:34 +0200

pastewebkit (0.9-3) unstable; urgency=low

  * Update for the new python policy
  * Convert to python-central

 -- Piotr Ozarowski <ozarow@gmail.com>  Fri, 16 Jun 2006 03:10:36 +0200

pastewebkit (0.9-2) unstable; urgency=low

  * Common paste* modules file moved to paste-common package

 -- Piotr Ozarowski <ozarow@gmail.com>  Sun, 14 May 2006 03:40:22 +0200

pastewebkit (0.9-1) unstable; urgency=low

  * Initial release (closes: #366188)

 -- Piotr Ozarowski <ozarow@gmail.com>  Wed, 10 May 2006 18:57:29 +0200
